# Menu Link Fragment

This module allows you to add fragment attributes to your menu links.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/menu_link_fragment).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/menu_link_fragment).


## Contents of this file

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

No extra module is required.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

To enable menu_link_fragment:

1. Enable Menu Link Fragment module
1. You can see the Link Fragment field in the Menu Link Items.
1. Smooth scrolling when clicking any anchor link added.
1. Nothing else :)


## Troubleshooting

Menu Link Fragment module doesn't provide any visible functions to the user
on its own, it just provides security handling services.


## Maintainers

- Manikandan Ramakrishnan - [Manikandan Era](https://www.drupal.org/u/manikandan-era)
