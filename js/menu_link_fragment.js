/**
 * @file
 * Contains main module script functions.
 * More info: https://www.drupal.org/user/3508408/
 */
(function ($, Drupal) {
  "use strict";

  /**
   * Drupal behavior to handle fragment anchor link.
   */
  Drupal.behaviors.menu_link_fragment = {
    attach: function (context, settings) {

      $(document).ready(function(){
        $('a[href^="#"]').on('click',function (e) {
            e.preventDefault();
            var target = this.hash;
            var $target = $(target);
            $('html, body').stop().animate({
              'scrollTop': $target.offset().top
            }, 900, 'swing', function () {
              // window.location.hash = target;
            });
        });
      });

    }
  };

})(jQuery, Drupal);
